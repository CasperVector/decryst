#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "rng.h"
#include "rbtree.h"
#include "metric.h"
#include "utils.h"
#include "cryst.h"
#ifdef BENCHMARK
#include "bench_metric.h"
#include "bench_cryst.h"
#endif
#include "int_cryst.h"

struct cr_wptr {
	struct affine *affs;
	unsigned (*aids)[3];
};

static void *cp_alloc (void const *src, unsigned n) {
	void *buf;
	if (!(buf = malloc (n))) return NULL;
	memcpy (buf, src, n);
	return buf;
}

float torus_pos (float x) {
	return x > 0.0 ? x - (signed) x : x + (signed) (-x) + 1;
}

static float kx_dot (signed const k[3], float const x[3]) {
	float ret = 0.0;
	for (unsigned i = 0; i < 3; ++i) ret += k[i] * x[i];
	return 2.0 * PI * ret;
}

static float aff_dot (struct affine const *aff, float const vec[3]) {
	float ret = aff->move;
	struct linear const *lin = aff->lin;
	for (unsigned i = 0; i < aff->n; ++i) {
		ret += vec[lin[i].axis] * lin[i].zoom;
	}
	return ret;
}

static float orig_weight (
	signed const hkl[3], float const origs[][3], float nOrig
) {
	float ret[2] = { 0.0, 0.0 };
	for (unsigned i = 0; i < nOrig; ++i) {
		float kx = kx_dot (hkl, origs[i]);
		ret[0] += cos (kx);
		ret[1] += sin (kx);
	}
	return ret[0] * ret[0] + ret[1] * ret[1];
}

static void balls_eval (
	struct crystal *cr, struct cr_atom *atom, unsigned nn[2]
) {
	struct cr_scat *scats = atom->scats;
	struct affine *affs = atom->affs;
	float (*origs)[3] = cr->origs;
	unsigned (*aids)[3] = atom->aids, i, j, k;
	nn[0] = atom->wyck[2] + 1;
	nn[1] = nn[0] * cr->nOrig;
	for (i = 0; i < atom->wyck[1]; ++i) {
		struct cr_ball *balls = scats[i].balls;
		for (j = 0; j < 3; ++j) {
			balls->xyz[j] = torus_pos (aff_dot (affs + aids[i][j], atom->xyz));
			if (cr->scales[2] == 0.0) continue;
			struct cr_ball *ball = balls;
			for (k = 1; k < cr->nOrig; ++k) {
				ball += nn[0];
				ball->xyz[j] = torus_pos (balls->xyz[j] + origs[k][j]);
			}
			if (atom->wyck[2]) {
				ball = balls;
				for (k = 0; k < cr->nOrig; ++k, ball += 2) {
					ball[1].xyz[j] = torus_pos (-ball[0].xyz[j]);
				}
			}
		}
	}
}

void atom_eval (struct crystal *cr, struct cr_atom *atom) {
	struct cr_peak *peaks = cr->hills->peaks;
	unsigned nn[2], i, j;
	balls_eval (cr, atom, nn);
	for (i = 0; i < atom->wyck[1]; ++i) {
		struct cr_scat *scat = atom->scats + i;
		struct cr_ball *balls = scat->balls;
		for (j = 0; j < nn[1]; ++j) balls[j].flag |= 0x1;
		for (j = 0; j < cr->nHill[1]; ++j) {
			struct cr_peak *peak = peaks + j;
			float sasf = cr->scales[0] * atom->asfs[j],
				kx = kx_dot (peak->peak.hkl, balls->xyz);
			signed *psc = peak->sc, *bsc = scat->scs[j];
			if (atom->wyck[2]) {
				psc[0] -= bsc[0];
				bsc[0] = 2 * sasf * cos (kx);
				psc[0] += bsc[0];
			} else {
				psc[0] -= bsc[0];
				psc[1] -= bsc[1];
				bsc[0] = sasf * cos (kx);
				bsc[1] = sasf * sin (kx);
				psc[0] += bsc[0];
				psc[1] += bsc[1];
			}
		}
	}
}

#ifdef BENCHMARK
void atom_eval_bb (struct crystal *cr, struct cr_atom *atom, int sc) {
	struct cr_peak *peaks = cr->hills->peaks;
	struct cr_scat *scats = atom->scats;
	unsigned nn[2], i, j, k;
	balls_eval (cr, atom, nn);
	for (i = 0; i < atom->wyck[1]; ++i) {
		struct cr_ball *balls = scats[i].balls;
		if (sc) for (j = 0; j < cr->nHill[1]; ++j) {
			struct cr_peak *peak = peaks + j;
			float asf = atom->asfs[j],
				kx = kx_dot (peak->peak.hkl, balls->xyz);
			if (atom->wyck[2]) peak->bsc[0] += 2 * asf * cos (kx);
			else {
				peak->bsc[0] += asf * cos (kx);
				peak->bsc[1] += asf * sin (kx);
			}
		} else for (j = 0; j < nn[1]; ++j) {
			struct cr_ball *ball = balls + j;
			for (k = 0; k < 3; ++k) {
				ball->bound[k][0] = torus_pos (ball->xyz[k] - atom->wid[k]);
				ball->bound[k][1] = torus_pos (ball->xyz[k] + atom->wid[k]);
			}
		}
	}
}
#endif

static struct crystal *cr_self_mk (
	struct hill_t const hills[], float const origs[][3],
	unsigned const wycks[][3], struct atom_t const atoms[],
	float const abc[3], float const angles[3], float const ctl[3],
	rng_t *rng, unsigned nHill, unsigned nOrig, unsigned nAtom
) {
	struct crystal *cr;
	unsigned i, j;
	if (!(cr = malloc (sizeof (struct crystal)))) goto cr_err;

	*cr = (struct crystal) {
		.nHill = { nHill, 0 }, .nAtom = { nAtom, 0 },
		.ctl = { ctl[1], ctl[2], 1.0 / (ctl[2] - ctl[1]) },
		.scores = { 0.0 }, .nOrig = nOrig, .rng = rng
	};
	for (i = 0; i < nHill; ++i) cr->nHill[1] += hills[i].n;
	for (cr->nPos[0] = i = 0; i < nAtom; ++i) for (j = 0; j < 3; ++j) {
		if (atoms[i].bound[j][0] != atoms[i].bound[j][1]) ++cr->nPos[0];
	}
	for (i = 0; i < nAtom; ++i) {
		unsigned const *wyck = wycks[atoms[i].wyck];
		cr->nAtom[1] += nOrig * wyck[1] * (wyck[2] + 1);
	}
	cr->scales[1] = 0.99 * INT_MAX / (cr->nAtom[1] * (cr->nAtom[1] - 1));
	cr->scales[2] = ctl[0];

	if (!rbforest_mk (&cr->bump, cr->nAtom[1])) goto bump_err;
	if (!(
		cr->poss = calloc (cr->nPos[0], sizeof (struct cr_pos))
	)) goto poss_err;
	if (!(
		cr->perm = calloc (cr->nPos[0], sizeof (unsigned))
	)) goto perm_err;
	if (!(
		cr->origs = cp_alloc (origs, nOrig * 3 * sizeof (float))
	)) goto origs_err;
	if (!(cr->met = metric_mk (abc, angles))) goto met_err;

	for (i = 0; i < cr->nPos[0]; ++i) cr->perm[i] = i;
	if (cr->nPos[0]) rng_shuf (rng, cr->perm, cr->nPos[0]);
	cr->nPos[1] = 0;

	return cr;
	free (cr->met); met_err:
	free (cr->origs); origs_err:
	free (cr->perm); perm_err:
	free (cr->poss); poss_err:
	rbforest_fin (&cr->bump); bump_err:
	free (cr); cr_err:
	return NULL;
}

static void cr_self_fin (struct crystal *cr) {
	free (cr->met);
	free (cr->origs);
	free (cr->perm);
	free (cr->poss);
	rbforest_fin (&cr->bump);
	free (cr);
}

static int cr_hills_mk (
	struct crystal *cr, struct peak_t const peaks[],
	struct hill_t const hills[]
) {
	if (!(
		cr->hills = calloc (cr->nHill[0], sizeof (struct cr_hill))
	)) goto hills_err;
	if (!(
		cr->hills->peaks = calloc (cr->nHill[1], sizeof (struct cr_peak))
	)) goto peaks_err;

	struct cr_peak *crPeaks = cr->hills->peaks;
	float sum = 0.0;
	unsigned i;

	for (i = 0; i < cr->nHill[0]; ++i) {
		if (i) cr->hills[i].peaks = cr->hills[i - 1].peaks + hills[i - 1].n;
		cr->hills[i].hill = hills[i];
		sum += hills[i].val0;
	}
	for (i = 0; i < cr->nHill[0]; ++i) cr->hills[i].hill.val0 /= sum;
	for (i = 0; i < cr->nHill[1]; ++i) {
		crPeaks[i].peak = peaks[i];
		crPeaks[i].peak.weight *= orig_weight
			(crPeaks[i].peak.hkl, cr->origs, cr->nOrig);
	}

	return 1;
	free (cr->hills->peaks); peaks_err:
	free (cr->hills); hills_err:
	return 0;
}

static void cr_hills_fin (struct crystal *cr) {
	free (cr->hills->peaks);
	free (cr->hills);
}

static int cr_atoms_mk (
	struct crystal *cr, struct affine const affs[], unsigned const aids[][3],
	unsigned const wycks[][3], float const dists[], float const asfs[],
	struct atom_t const atoms[], unsigned nWyck, unsigned nElem
) {
	struct cr_scat *scats;
	unsigned i, j;

	for (j = i = 0; i < nWyck; ++i) j += wycks[i][0];
	if (!(
		cr->affs = cp_alloc (affs, j * sizeof (struct affine))
	)) goto affs_err;
	for (j = i = 0; i < nWyck; ++i) j += wycks[i][1];
	if (!(cr->aids = cp_alloc (aids, j * 3 * sizeof (unsigned)))) goto aids_err;

	if (!(
		cr->dists = cp_alloc (dists, nElem * nElem * sizeof (float))
	)) goto dists_err;
	if (!(cr->asfs = cp_alloc (
		asfs, nElem * cr->nHill[1] * sizeof (float)
	))) goto asfs_err;

	if (!(
		cr->atoms = calloc (cr->nAtom[0], sizeof (struct cr_atom))
	)) goto atoms_err;
	for (j = i = 0; i < cr->nAtom[0]; ++i) j += wycks[atoms[i].wyck][1];
	if (!(
		scats = cr->atoms->scats = calloc (j, sizeof (struct cr_scat))
	)) goto scats_err;

	if (!(scats->scs = calloc (
		j * cr->nHill[1], 2 * sizeof (signed)
	))) goto scs_err;
	if (!(
		scats->balls = calloc (cr->nAtom[1], sizeof (struct cr_ball))
	)) goto balls_err;

	return 1;
	free (cr->atoms->scats->balls); balls_err:
	free (cr->atoms->scats->scs); scs_err:
	free (cr->atoms->scats); scats_err:
	free (cr->atoms); atoms_err:
	free (cr->asfs); asfs_err:
	free (cr->dists); dists_err:
	free (cr->aids); aids_err:
	free (cr->affs); affs_err:
	return 0;
}

static void cr_atoms_fin (struct crystal *cr) {
	free (cr->atoms->scats->balls);
	free (cr->atoms->scats->scs);
	free (cr->atoms->scats);
	free (cr->atoms);
	free (cr->asfs);
	free (cr->dists);
	free (cr->aids);
	free (cr->affs);
}

static int cr_atoms_init (
	struct crystal *cr, unsigned const wycks[][3], struct atom_t const atoms[],
	unsigned nWyck, unsigned nElem
) {
	struct cr_atom *crAtoms = cr->atoms;
	struct cr_wptr *wps;
	unsigned i, j, k;

	if (!(wps = calloc (nWyck, sizeof (struct cr_wptr)))) return 0;
	wps[0] = (struct cr_wptr) { .affs = cr->affs, .aids = cr->aids };
	for (i = 1; i < nWyck; ++i) wps[i] = (struct cr_wptr) {
		.affs = wps[i - 1].affs + wycks[i - 1][0],
		.aids = wps[i - 1].aids + wycks[i - 1][1]
	};

	for (i = 0; i < cr->nAtom[0]; ++i) {
		struct cr_atom *atom = crAtoms + i;
		struct cr_scat *scat;
		unsigned elem = atoms[i].elem, wyck = atoms[i].wyck;
		if (i) {
			struct cr_atom *prev = atom - 1;
			scat = atom->scats = prev->scats + prev->wyck[1];
			scat->scs = prev->scats->scs + prev->wyck[1] * cr->nHill[1];
			scat->balls = prev->scats->balls + k;
		}

		atom->asfs = cr->asfs + elem * cr->nHill[1];
		memcpy (atom->wyck, wycks[wyck], 3 * sizeof (unsigned));
		atom->affs = wps[wyck].affs;
		atom->aids = wps[wyck].aids;
		k = cr->nOrig * (atom->wyck[2] + 1);
		for (j = 1; j < atom->wyck[1]; ++j) {
			scat = atom->scats + j;
			scat->scs = (scat - 1)->scs + cr->nHill[1];
			scat->balls = (scat - 1)->balls + k;
		}

		scat = atom->scats;
		k *= atom->wyck[1];
		for (j = 0; j < k; ++j) scat->balls[j] = (struct cr_ball)
			{ .dists = cr->dists + elem * nElem, .elem = elem };
		scat->balls->n = k;
		scat->balls->flag = 0x2;
	}

	free (wps);
	return 1;
}

static void cr_scale0_init (struct crystal *cr) {
	float sum = 0.0;
	for (unsigned i = 0; i < cr->nHill[1]; ++i) {
		float s = 0.0;
		for (unsigned j = 0; j < cr->nAtom[0]; ++j) {
			unsigned *wyck = cr->atoms[j].wyck;
			s += cr->atoms[j].asfs[i] * wyck[1] * (wyck[2] + 1);
		}
		if (sum < s) sum = s;
	}
	cr->scales[0] = 0.99 * INT_MAX / sum;
}

static void cr_poss_init (struct crystal *cr, struct atom_t const atoms[]) {
	for (unsigned i = 0, j = 0; j < cr->nAtom[0]; ++j) {
		struct atom_t const *atom = atoms + j;
		struct cr_atom *crAtom = cr->atoms + j;
		int flag = 0;
		for (unsigned k = 0; k < 3; ++k) {
			if (atom->bound[k][0] == atom->bound[k][1]) {
				crAtom->xyz[k] = atom->bound[k][0];
			} else {
				struct cr_pos *pos = cr->poss + i;
				*pos = (struct cr_pos) {
					.atom = crAtom, .axis = k, .flag = 1, .bound = {
						atom->bound[k][0], atom->bound[k][1],
						atom->bound[k][1] - atom->bound[k][0]
					}
				};
				pos->pos[0] = pos->pos[1] =
					pos->bound[0] + pos->bound[2] * rng_float (cr->rng);
				flag = 1;
				++i;
			}
		}
		if (!flag) atom_eval (cr, crAtom);
	}
}

struct crystal *cryst_mk (
	struct peak_t const peaks[], struct hill_t const hills[],
	float const origs[][3], struct affine const affs[],
	unsigned const aids[][3], unsigned const wycks[][3],
	float const dists[], float const asfs[], struct atom_t const atoms[],
	float const abc[3], float const angles[3], float const ctl[3],
	rng_t *rng, unsigned nHill, unsigned nOrig,
	unsigned nWyck, unsigned nElem, unsigned nAtom
) {
	struct crystal *cr;

	if (!(cr = cr_self_mk (
		hills, origs, wycks, atoms, abc, angles,
		ctl, rng, nHill, nOrig, nAtom
	))) goto cr_err;
	if (!cr_hills_mk (cr, peaks, hills)) goto hills_err;
	if (!cr_atoms_mk (
		cr, affs, aids, wycks, dists, asfs, atoms, nWyck, nElem
	)) goto atoms_err;

	if (!cr_atoms_init (cr, wycks, atoms, nWyck, nElem)) goto init_err;
	cr_scale0_init (cr);
	cr_poss_init (cr, atoms);

	return cr; init_err:
	cr_atoms_fin (cr); atoms_err:
	cr_hills_fin (cr); hills_err:
	cr_self_fin (cr); cr_err:
	return NULL;
}

void cryst_fin (struct crystal *cr) {
	cr_atoms_fin (cr);
	cr_hills_fin (cr);
	cr_self_fin (cr);
}

#ifdef BENCHMARK
int cryst_bmk (
	struct crystal *cr, float const rads[],
	struct peak_t const peaks[], struct atom_t const atoms[],
	float const abc[3], float const angles[3], unsigned nElem
) {
	float ratios[3];
	unsigned axes = 0x0, i, j;

	ratios_get (abc, angles, ratios);
	for (j = 0, i = 1; i < 3; ++i) if (ratios[i] < ratios[j]) j = i;
	axes = 0x1 << j;
	if (!(cr->bbump = bump_mk (axes, cr->nAtom[1]))) goto bbump_err;
	if (!(cr->wids = calloc (nElem, 3 * sizeof (float)))) goto wids_err;

	for (i = 0; i < nElem; ++i) {
		for (j = 0; j < 3; ++j) if (
			(cr->wids[i][j] = cr->ctl[1] * ratios[j] * rads[i]) >= 0.4999
		) cr->wids[i][j] = 0.4999;
	}
	for (i = 0; i < cr->nAtom[0]; ++i) {
		cr->atoms[i].wid = cr->wids[atoms[i].elem];
	}

	return 1;
	free (cr->wids); wids_err:
	bump_fin (cr->bbump); bbump_err:
	return 0;
}

void bryst_fin (struct crystal *cr) {
	free (cr->wids);
	bump_fin (cr->bbump);
	cryst_fin (cr);
}
#endif

