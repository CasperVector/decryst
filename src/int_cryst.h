struct cr_peak {
	struct peak_t peak;
#ifdef BENCHMARK
	float bsc[2];
#endif
	signed sc[2];
};

struct cr_hill {
	struct hill_t hill;
	struct cr_peak *peaks;
	float val;
};

struct cr_ball {
	float *dists, xyz[3];
#ifdef BENCHMARK
	float bound[3][2];
#endif
	unsigned flag, elem, n;
};

struct cr_scat {
	struct cr_ball *balls;
	signed (*scs)[2];
};

struct cr_atom {
	struct affine *affs;
	struct cr_scat *scats;
	float *asfs, xyz[3];
#ifdef BENCHMARK
	float *wid;
#endif
	unsigned (*aids)[3], wyck[3];
	int flag;
};

struct cr_pos {
	float pos[2], bound[3];
	struct cr_atom *atom;
	unsigned axis, flag;
};

struct crystal {
	struct rbforest bump;
#ifdef BENCHMARK
	struct bump_t *bbump;
#endif
	rng_t *rng;
	metric *met;
	struct cr_atom *atoms;
	struct cr_pos *poss;
	struct cr_hill *hills;
	struct affine *affs;
	unsigned *perm, (*aids)[3], nHill[2], nAtom[2], nPos[2], nOrig;
#ifdef BENCHMARK
	float (*wids)[3];
#endif
	float *dists, *asfs, (*origs)[3], ctl[3], scales[3], scores[3];
};

#ifndef BENCHMARK
typedef int narrow_f (crystal const *, unsigned, unsigned, signed *);
#endif

extern float torus_pos (float x);
extern void atom_eval (crystal *cr, struct cr_atom *atom);
#ifdef BENCHMARK
extern void atom_eval_bb (crystal *cr, struct cr_atom *atom, int sc);
#endif

