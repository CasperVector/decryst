extern unsigned ns_delta (struct timespec const *t1, struct timespec const *t2);
extern void stat_step (float stat[2], float x);
extern void stat_write (float const stat[2], unsigned const n[2]);

